package com.transfet.social.student;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.ReportingPolicy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * Mapping instances from {@link StudentRegistrationRequest} to {@link Student}.
 */
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public abstract class StudentMapper {

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Mapping(source = "password", target = "password", qualifiedByName = "passwordMapper")
    abstract Student mapToStudent(StudentRegistrationRequest registrationRequest);

    abstract StudentDTO mapToStudentDTO(Student student);

    @Named(value = "passwordMapper")
    public String map(String password) {
        return bCryptPasswordEncoder.encode(password);
    }
}
