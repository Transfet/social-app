package com.transfet.social.student;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

/**
 * Repository for {@link Role}.
 */
public interface RoleRepository extends JpaRepository<Role, Long> {

    Role findByName(String name);

    @Transactional
    @Modifying
    @Query(value = "insert into student_role_sw (role_id, student_id) VALUES (?1, ?2)", nativeQuery = true)
    void addRoleToStudent(Long roleId, Long studentId);
}
