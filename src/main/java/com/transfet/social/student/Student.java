package com.transfet.social.student;

import com.transfet.social.location.UserGeoLocation;
import lombok.Data;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Domain object for a student.
 */
@Data
@Entity
@Table(name = "student")
@ToString
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private String userName;
    private String lastName;
    private String firstName;
    private String password;
    private String email;

    private LocalDateTime connectedTime;

    private Boolean isActive;

    private Integer startYear;
    private Integer courseId;
    private Integer facultyId;

    private String profileImage;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "student_role_sw", inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"),
            joinColumns = @JoinColumn(name = "student_id", referencedColumnName = "id"))
    private List<Role> roles;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "student_geo_location_sw",
            inverseJoinColumns = @JoinColumn(name = "location_id", referencedColumnName = "id"),
            joinColumns = @JoinColumn(name = "student_id", referencedColumnName = "id"))
    private UserGeoLocation geoLocation;

}
