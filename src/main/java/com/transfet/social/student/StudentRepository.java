package com.transfet.social.student;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository for {@link Student}.
 */
public interface StudentRepository extends JpaRepository<Student, Long> {

    Student findByUserName(String userName);
}
