package com.transfet.social.student;

import com.transfet.social.location.UserGeoLocation;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * DTO object for a {@link Student}.
 */
@Setter
@Getter
public class StudentDTO {

    private String userName;
    private String lastName;
    private String firstName;
    private String email;
    private LocalDateTime connectedTime;
    private Boolean isActive;
    private Integer startYear;
    private Integer courseId;
    private Integer facultyId;
    private String profileImage;
    private UserGeoLocation geoLocation;
}
