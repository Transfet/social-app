package com.transfet.social.student;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

/**
 * Domain object for a student who will be registered.
 */
@Getter
@Setter
public class StudentRegistrationRequest {

    @Size(max = 20)
    private String userName;

    @Size(max = 20)
    private String lastName;

    @Size(max = 20)
    private String firstName;

    @Size(min = 4)
    private String password;

    @Email
    private String email;

    private String startYear;

    private String courseId;

    private Integer facultyId;
}
