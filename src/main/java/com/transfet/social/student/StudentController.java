package com.transfet.social.student;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * Rest controller for {@link Student}.
 */
@RestController
@RequestMapping("/student")
@Slf4j
@AllArgsConstructor
public class StudentController {

    private StudentService studentService;
    private StudentMapper studentMapper;

    @PostMapping(value = "/save", consumes = "application/json")
    public ResponseEntity registerStudent(@RequestBody @Valid StudentRegistrationRequest request) {

        log.info("[POST] /users/save");
        log.info("[POST] Registering a new user");

        try {

            if (!studentService.isUserNameIsAvailable(request.getUserName())) {
                return new ResponseEntity<>("Username already exists", HttpStatus.CONFLICT);

            } else {
                studentService.saveStudent(studentMapper.mapToStudent(request));
            }

        } catch (Exception e) {
            log.error("Exception during user registration with message={}", e.getMessage());
            e.printStackTrace();
        }

        log.info("[POST] New user is registered");
        return new ResponseEntity<>("OK", HttpStatus.OK);
    }

    @GetMapping(path = "/actual")
    public ResponseEntity<StudentDTO> getActualUser() {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Student student = studentService.getStudentByUserName(authentication.getName());

        log.info("[GET] get number {} user's information", student.getId());

        return ResponseEntity.ok(studentMapper.mapToStudentDTO(student));
    }

}
