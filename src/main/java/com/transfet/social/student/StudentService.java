package com.transfet.social.student;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * Service for {@link Student}.
 */
@AllArgsConstructor
@Service
@Slf4j
public class StudentService {

    private StudentRepository studentRepository;
    private RoleRepository roleRepository;

    public Student getStudentByUserName(String userName) {
        return studentRepository.findByUserName(userName);
    }

    Boolean isUserNameIsAvailable(String userName) {
        return Objects.isNull(studentRepository.findByUserName(userName));
    }

    Student saveStudent(Student student) {

        Student savedStudent = studentRepository.saveAndFlush(student);
        log.info("student = {}", savedStudent);

        try {

            Role userRole = roleRepository.findByName("ROLE_USER");
            roleRepository.addRoleToStudent(userRole.getId(), savedStudent.getId());

        } catch (Exception e) {
            log.error("Exception while saving user with message={}", e.getMessage());
            e.printStackTrace();
        }

        return savedStudent;
    }
}
