package com.transfet.social.location;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Domain object for a geo location which tells us the student location.
 */
@Data
@Entity
@Table(name = "geo_location")
public class UserGeoLocation {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private Double latitude;
    private Double longitude;
    private Double radius;
}
