package com.transfet.social.security;

import com.transfet.social.student.Student;
import com.transfet.social.student.StudentService;
import lombok.AllArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Collections;
import java.util.Objects;

/**
 * Custom implementation of {@link UserDetailsManager}.
 */
@Component
@AllArgsConstructor
public class StudentDetailsManager implements UserDetailsManager {

    private StudentService studentService;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {

        Student student = studentService.getStudentByUserName(userName);
        Collection<GrantedAuthority> grantedAuthorities = Collections.singletonList((GrantedAuthority) () -> "ROLE_USER");
        return new User(student.getUserName(), student.getPassword(), grantedAuthorities);
    }

    @Override
    public void createUser(UserDetails userDetails) {
    }

    @Override
    public void updateUser(UserDetails userDetails) {
    }

    @Override
    public void deleteUser(String s) {
    }

    @Override
    public void changePassword(String s, String s1) {
    }


    @Override
    public boolean userExists(String userName) {
        return Objects.nonNull(studentService.getStudentByUserName(userName));
    }
}
