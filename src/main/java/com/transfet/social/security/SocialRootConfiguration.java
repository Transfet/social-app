package com.transfet.social.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Web configuration for the application.
 */
@Configuration
@EnableWebMvc
public class SocialRootConfiguration extends WebMvcConfigurerAdapter {

}
