package com.transfet.social.course;

/**
 * Enumeration for course types.
 */
public enum CourseType {

   BSC("Bachelor of Science"),

   MSC("Master of Sciences");

   private String typeFullName;

   public String getTypeFullName() {
       return typeFullName;
   }

   CourseType(String typeFullName) {
       this.typeFullName = typeFullName;
   }
}
