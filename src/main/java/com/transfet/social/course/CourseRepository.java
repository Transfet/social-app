package com.transfet.social.course;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository for {@link Course}.
 */
interface CourseRepository extends JpaRepository<Course, Long> {
}
