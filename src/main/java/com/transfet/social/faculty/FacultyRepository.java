package com.transfet.social.faculty;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository for {@link Faculty}.
 */
public interface FacultyRepository extends JpaRepository<Faculty, Long> {
}
