package com.transfet.social;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Main class of the applcation.
 */
@SpringBootApplication
public class SocialApplication {

    public static void main(String... args) {
        SpringApplication.run(SocialApplication.class, args);
    }
}
