CREATE SEQUENCE hibernate_sequence AS BIGINT INCREMENT BY 1 START WITH 1 CACHE 500;


create table role
(
    id   bigint not null,
    name varchar(255),
    primary key (id)
);
create table student
(
    id             bigint not null,
    connected_time timestamp,
    course_id      bigint,
    email          varchar(255),
    faculty_id     bigint,
    first_name     varchar(255),
    is_active      boolean,
    last_name      varchar(255),
    password       varchar(255),
    profile_image  varchar,
    start_year     bigint,
    user_name      varchar(255),
    primary key (id)
);
create table course
(
    id          bigint not null,
    course_type bigint,
    description varchar(255),
    name        varchar(255),
    short_name  varchar(255),
    primary key (id)
);
create table faculty
(
    id         bigint not null,
    name       varchar(255),
    short_name varchar(255),
    primary key (id)
);
create table faculty_course_sw
(
    faculty_id bigint not null,
    course_id  bigint not null,
    FOREIGN KEY (faculty_id) REFERENCES faculty (id),
    FOREIGN KEY (course_id) REFERENCES course (id)
);
create table geo_location
(
    id        bigint not null,
    latitude  double precision,
    longitude double precision,
    radius    double precision,
    primary key (id)
);

create table student_geo_location_sw
(
    location_id bigint,
    student_id  bigint not null,
    FOREIGN KEY (location_id) REFERENCES geo_location (id),
    FOREIGN KEY (student_id) REFERENCES student (id)
);

create table student_role_sw
(
    student_id bigint not null,
    role_id    bigint not null,
    FOREIGN KEY (student_id) REFERENCES student (id),
    FOREIGN KEY (role_id) REFERENCES role (id)
);

INSERT INTO role(id, name)
VALUES (1, 'ROLE_USER');