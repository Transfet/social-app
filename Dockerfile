FROM adoptopenjdk/openjdk11:latest

RUN mkdir /opt/social
COPY /build/libs/social-1.0-SNAPSHOT.jar /opt/social/app.jar

EXPOSE 8080

CMD ["java", "-jar", "/opt/gradyent/app.jar"]
